# Embedded Unit Testing SWDS Source Code

This repository contains all sample code used for the SWDS presentation on 
embedded unit testing.

## Zune Bug

This subdirectory contains sample code used during the presentation with tests 
for the Zune leap year bug. Note that this bug exposes itself with an infinte 
loop, so take care when running the tests (running with gdb will help you find 
the issue in the code).

## Utility

This is an example project for which developers can feel free to try and write 
tests. The provided code has a small bug in it that unit tests should be able 
to find.

# Getting the software

This repository references the CppUTest framework as a submodule. After 
cloning, the following commands will get the necessary code from github 
(requires git version 1.7.12 or later):

    git submodule init && git submodule update

# Building the software

The root directory contains a makefile that will build the tests for all the 
projects. In addition, users can go into the particular subdirectory of the 
project they are interested in working with and run the tests for only that 
project. 

## Root Makefile Targets

The following make targets exist which build the 'all' target of the 
respective subdirectory:

    make zune
    make example

An `all` and `clean` target exists as well for building both subdirectories and 
cleaning them respectively.

## Submakefile Targets

Each example has sample code that run the same code under test with and without 
a testing framework. These targets are as follows:

    make withfwk
    make nofwk
    
As before,  `all` and `clean` targets also exist as well. If you build a 
version that depends on the CppUTest framework, the makefile will automatically 
build the framework for you (be sure to read the section above about getting 
the software).
