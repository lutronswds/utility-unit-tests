/*
 * Utility.cpp
 *
 *  Created on: Oct 22, 2013
 *      Author: jlenz
 */

#include <stdint.h>
#include "Utility.h"

/**
 * @brief Compares the memory pointed to by two pointers for a given length.
 * @param[in] Src1 pointer to first memory address to be compared.
 * @param[in] Src2 pointer to second memory address to be compared.
 * @param[in] Length number of bytes to be compared between the two memory addresses.
 * @return @c OS_UTILITY_COMPARE_LESS if the first different byte of Src1 is less than Src2.
 * @return @c OS_UTILITY_COMPARE_EQUAL if the memory for Src1 and Src2 are equal. 
 * @return @c OS_UTILITY_COMPARE_LESS if the first different byte of Src1 is greater than Src2.
 */
extern "C" int UtilityMemCompare(const void *Src1, const void *Src2, size_t Length)
{
   uint8_t const * first1 = static_cast<uint8_t const *>(Src1);
   uint8_t const * last1 = static_cast<uint8_t const *>(Src1) + Length;
   uint8_t const * first2 = static_cast<uint8_t const *>(Src2);
   
   while ((first1 != last1) && (*first1 == *first2))
   {
      ++first1; ++first2;
   }
   
   if (*first1 < *first2)
   {
      return UTILITY_COMPARE_LESS;
   }
   else if (*first1 == *first2)
   {
      return UTILITY_COMPARE_EQUAL;
   }
   else
   {
      return UTILITY_COMPARE_GREATER;
   }
}
