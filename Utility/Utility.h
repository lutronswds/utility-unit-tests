/*
 * Utility.h
 *
 *  Created on: Oct 22, 2013
 *      Author: jlenz
 */

#ifndef UTILITY_H_
#define UTILITY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

#define UTILITY_COMPARE_LESS (-1)
#define UTILITY_COMPARE_EQUAL (0)
#define UTILITY_COMPARE_GREATER (1)

int UtilityMemCompare(const void *Src1, const void *Src2, size_t Length);

#ifdef __cplusplus
}
#endif

#endif /* UTILITY_H_ */
