# Embedded Unit Testing Example

This project contains a short representative example of unit testable software 
that could run on an embedded device. The code under test contains a simple 
memory compare function that is supposed to return specific values based on the 
parameters passed in (one return value for greater, less, or equal to). There 
is a bug in the software that causes the function to not work correctly in some 
cases. Developers can choose to write their own tests from scratch or use the 
CppUTest framework to help write tests.

# Getting the software

This repository references the CppUTest framework as a submodule. After 
cloning, the following commands will get the necessary code from github 
(requires git version 1.7.12 or later):

    git submodule init && git submodule update

# Building the software

Users are more than welcome to write their own test software to get experince 
with unit testing. A file with a `main()` function is provided as an entry point 
to the user's tests. TODO: Develop this.

The provided makefile will automatically make the test framework the first 
time you build (this will take a minute or two depending on your development 
environment).

## Makefile targets

The following will make bootstrapped tests that do not require the CppUTest 
framework.

    make nofwk

The following will make bootstrapped tests that use the CppUTest framework. 
The test will fail at the first test where developers can start writing tests.

    make withfwk

The all & default target will make both of these variants.
