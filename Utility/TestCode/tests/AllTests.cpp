/*
 * AllTests.cpp
 *
 *  Created on: Jun 1, 2013
 *      Author: Jonathan Lenz
 */

#include "CppUTest/CommandLineTestRunner.h"

int main(int ac, char** av)
{
   return CommandLineTestRunner::RunAllTests(ac, av);
}



