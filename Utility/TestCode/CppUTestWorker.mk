# Defining this as "@" suppresses command line output. Clear if you wish to see 
# more information such as how the compiler is being invoked.
SILENCE = @

ifeq ($(CPPUTEST_HOME),)
$(error CPPUTEST directory not specified. Compilation terminated.)
endif

# Name of component/module under test.
COMPONENT_NAME = Utility

# Directories containing files under test. Note that this variable assumes 
# that ALL source files in this directory are under the same test.  Leave 
# blank if this is not true. 
SRC_DIRS = 

# Specific files that are part of the test. Do not need to be added here if 
# they are in directories specified in SRC_DIRS.
SRC_FILES = Utility.cpp

# Directories containing files doing testing. Note that this variable assumes 
# that ALL source files in this directory are required for the test.  Leave 
# blank if this is not true. 
TEST_SRC_DIRS = 

# Specific files that are performing the test. Do not need to be added here if 
# they are in directories specified in TEST_SRC_DIRS.
TEST_SRC = TestCode/tests/AllTests.cpp TestCode/tests/UtilityTest.cpp

# Directories containing mock files for this test. Note that this variable 
# assumes that ALL source files in this directory are required for the test.  
# Leave blank if this is not true. 
MOCKS_SRC_DIRS =

# Specific mock files that are needed for test. Do not need to be added here if 
# they are in directories specified in MOCKS_SRC_DIRS.
MOCKS_SRC = 

# Paths that should be checked for include files for any include files called 
# out that are not part of the test framework.
INCLUDE_DIRS = .

# Lines below here configure CppUTest for our test setup.

# CppUTest Inputs
CPP_PLATFORM = Gcc
CPPUTEST_USE_EXTENSIONS = Y
CPPUTEST_USE_MEM_LEAK_DETECTION = Y
CPPUTEST_USE_GCOV = Y
CPPFLAGS += -I$(CPPUTEST_HOME)/include 

# Compiled Output Directories
CPPUTEST_BIN_DIR = bin/$(COMPONENT_NAME)_test
CPPUTEST_OBJS_DIR = $(CPPUTEST_BIN_DIR)/objs
CPPUTEST_LIB_DIR = $(CPPUTEST_BIN_DIR)/lib

# Allow access to testing framework
INCLUDE_DIRS += $(CPPUTEST_HOME)/include

# CppUTest Outputs
TEST_TARGET = $(CPPUTEST_BIN_DIR)/$(COMPONENT_NAME)_test

# Compiler Options
CPPUTEST_CFLAGS += -Wall -Wstrict-prototypes -std=c99 -mno-ms-bitfields
CPPUTEST_CXXFLAGS += -mno-ms-bitfields
CPPUTEST_WARNINGFLAGS = -Wall -Wextra -Wshadow -Wswitch-default

# Linker Options
LD_LIBRARIES = -L$(CPPUTEST_HOME)/lib -lCppUTest -lCppUTestExt

include $(CPPUTEST_HOME)/build/MakefileWorker.mk 
