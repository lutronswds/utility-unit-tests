/**
 * @file ZuneBug_LeapYear.cpp
 * @brief This file implements the isLeapYear function to illustrate the Zune
 *    bug that caused Zune devices to crash on Wednesday, December 31st, 2008.
 */

#include <iostream>
using namespace std;

#include "ZuneBug_LeapYear.h"

/**
 * @brief   Receives the \p year, returns TRUE if that year is a leap year.
 * @param   year - the year to evaluate
 * @return  TRUE if year arg is a leap year, FALSE if not
 */
bool IsLeapYear(uint32_t year)
{
   return ((year % 4 == 0 && year % 100 != 0) || ( year % 400 == 0));
}
