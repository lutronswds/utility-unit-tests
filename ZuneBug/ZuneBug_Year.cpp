/**
 * @file ZuneBug_Year.cpp
 * @brief This file implements the getYear function to illustrate the Zune bug
 *    that caused Zune devices to crash on Wednesday, December 31st, 2008.
 */

#include <iostream>
using namespace std;

#include "ZuneBug_Year.h"
#include "ZuneBug_LeapYear.h"

/**
 * @brief   Receives the \p days since ORIGINYEAR, returns the year.
 * @param   days - number of days since 1980 (1 = Jan 1st, 1980)
 * @return  year
 */
uint32_t GetYear(uint32_t days)
{
   uint32_t year = ORIGINYEAR; /* = 1980 */

   while (days > 365)
   {
      if (IsLeapYear(year))
      {
         if (days > 366)
         {
            days -= 366;
            year += 1;
         }
      }
      else
      {
         days -= 365;
         year += 1;
      }
   }
   return year;
}
