/**
 * @file ZuneBug_Year.h
 * @brief Header for the file that implements the GetYear function to
 *    illustrate the Zune bug that caused Zune devices to crash on Wednesday,
 *    December 31st, 2008.
 */

#ifndef ZUNEBUG_YEAR_H_
#define ZUNEBUG_YEAR_H_

#include <stdint.h>

#define ORIGINYEAR   1980

uint32_t GetYear(uint32_t days);

#endif
