/**
 * @file main.cpp
 * @brief This file implements the main function.
 */

#include <iostream>
using namespace std;

#include "ZuneBug_Year.h"

/**
 * @brief   Main function.  Calls the GetYear function once.
 */
int main(void)
{
   cout << "The year of day 500 is " << GetYear(500) << endl;
   return 0;
}
