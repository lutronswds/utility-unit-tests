SILENCE =

TARGET_NAME = Zune_NormalBuild
OBJS_DIR = bin/$(TARGET_NAME)
TEST_TARGET = $(OBJS_DIR)/$(TARGET_NAME)

SRC = ZuneBug_LeapYear.cpp ZuneBug_Year.cpp main.cpp
OBJ = $(addprefix $(OBJS_DIR)/,$(subst .c,.o,$(subst .cpp,.o, $(subst .S,.o,$(SRC)))))
DEP = $(addprefix $(OBJS_DIR)/,$(subst .c,.d,$(subst .cpp,.d, $(subst .S,.d,$(SRC)))))
INCLUDES = -I.

debug_print_list = $(foreach word,$1,echo " $(word)";) echo;

ifneq "$(MAKECMDGOALS)" "clean"
-include $(DEP)
endif

.SECONDARY: $(DEP) $(OBJ)

.PHONY: all
all: $(TEST_TARGET)
	@echo Running $(TARGET_NAME); echo
	$(SILENCE)./$(TEST_TARGET)

$(TEST_TARGET): $(OBJ) 
	@echo Linking $(notdir $@)
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(LINK.o) -g -o $@ $^ -lstdc++

$(OBJS_DIR)/%.o: %.cpp
	@echo Compiling $(notdir $<)
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(COMPILE.cpp) -g $(INCLUDES) -MMD -MP $(OUTPUT_OPTION) $<

$(OBJS_DIR)/%.o: %.c
	@echo Compiling $(notdir $<)
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(COMPILE.c) -g $(INCLUDES) -MMD -MP $(OUTPUT_OPTION) $<

.PHONY: debug
debug: 
	@echo
	@echo "Debug info for $(TARGET_NAME) target:"
	@echo "Target Source files:"
	@$(call debug_print_list,$(SRC))
	@echo "Target Linked Objects:"
	@$(call debug_print_list,$(OBJ))
	@echo "Includes:"
	@$(call debug_print_list,$(INCLUDES))
	@echo "Target Defines:"
	@$(call debug_print_list,$(DEFINES))
	@echo "Target Libraries:"
	@$(call debug_print_list,$(LD_LIBRARIES))

.PHONY: clean
clean:
	@echo Cleaning $(TARGET_NAME)...
	$(SILENCE)$(RM) -r $(OBJS_DIR)
