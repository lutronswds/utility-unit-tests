/**
 * @file ZuneBug_LeapYear.h
 * @brief Header for the file that implements the IsLeapYear function to
 *    illustrate the Zune bug that caused Zune devices to crash on Wednesday,
 *    December 31st, 2008.
 */

#ifndef ZUNEBUG_LEAPYEAR_H_
#define ZUNEBUG_LEAPYEAR_H_

#include <stdint.h>

bool IsLeapYear(uint32_t year);

#endif
