/**
 * @file ZuneBugTest_CppUTest.cpp
 * @brief This file tests the ZuneBug.cpp file using the CppUTest framework.
 */

#include "CppUTest/TestHarness.h"
#include "ZuneBug_Year.h"

TEST_GROUP(ZuneBug)
{
   /** @brief This function runs before every single test */
   void setup()
   {
   }

   /** @brief This function runs after every single test */
   void teardown()
   {
   }
};

TEST(ZuneBug, TestPass)
{
   //
}

/**
 * @brief   Tests if passing in 0 to the getYear function returns ORIGINYEAR
 */
TEST(ZuneBug, day0ReturnsOriginYear)
{
   LONGS_EQUAL(ORIGINYEAR, GetYear(0));
}

/**
 * @brief   Tests if passing in 100 to the getYear function returns ORIGINYEAR
 */
TEST(ZuneBug, day100ReturnsOriginYear)
{
   LONGS_EQUAL(ORIGINYEAR, GetYear(100));
}

/**
 * @brief   Tests passing in the last day of a leap year.
 */
TEST(ZuneBug, lastDayOfLeapYear)
{
   LONGS_EQUAL(ORIGINYEAR, GetYear(366));
}

/**
 * @brief   Tests passing in the first day after a leap year.
 */
TEST(ZuneBug, firstDayAfterLeapYear)
{
   LONGS_EQUAL(ORIGINYEAR + 1, GetYear(367));
}


