/**
 * @file ZuneBugTest.cpp
 * @brief This file tests the ZuneBug.cpp file.
 */
 
#include <iostream>
#include "ZuneBug_Year.h"

using namespace std;

static void test_day0ReturnsOriginYear(void);
static void test_day100ReturnsOriginYear(void);
static void test_lastDayOfLeapYear(void);
static void test_firstDayAfterLeapYear(void);

/**
 * @brief   Runs all of the tests
 */
int main(void)
{
   test_day0ReturnsOriginYear();
   test_day100ReturnsOriginYear();
   test_lastDayOfLeapYear();
   test_firstDayAfterLeapYear();
   return 0;
}

/**
 * @brief   Tests if passing in 0 to the getYear function returns ORIGINYEAR
 */
static void test_day0ReturnsOriginYear(void)
{
   uint32_t days = 0;
   uint32_t year = GetYear(days);
   if (year != ORIGINYEAR)
   {
      cout << "test_day0ReturnsOriginYear failed.  Expected " << 
	     ORIGINYEAR << ", actual " << year << endl;
   }
   else
   {
      cout << "test_day0ReturnsOriginYear passed." << endl;
   }
}

/**
 * @brief   Tests if passing in 100 to the getYear function returns ORIGINYEAR
 */
static void test_day100ReturnsOriginYear(void)
{
   uint32_t days = 100;
   uint32_t year = GetYear(days);
   if (year != ORIGINYEAR)
   {
      cout << "test_day100ReturnsOriginYear failed.  Expected " << 
	     ORIGINYEAR << ", actual " << year << endl;
   }
   else
   {
      cout << "test_day100ReturnsOriginYear passed." << endl;
   }
}

/**
 * @brief   Tests passing in the last day of a leap year.
 */
static void test_lastDayOfLeapYear(void)
{
   //ORIGINYEAR is a leap year.  365 days since ORIGINYEAR = December 31st
   uint32_t days = 366; 
   uint32_t year = GetYear(days);
   if (year != ORIGINYEAR)
   {
      cout << "test_lastDayOfLeapYear failed.  Expected " << 
	     ORIGINYEAR << ", actual " << year << endl;
   }
   else
   {
      cout << "test_lastDayOfLeapYear passed." << endl;
   }
}

/**
 * @brief   Tests passing in the first day after a leap year.
 */
void test_firstDayAfterLeapYear(void)
{
   //ORIGINYEAR is a leap year.  366 days since ORIGINYEAR = Jan 1st
   uint32_t days = 367; 
   uint32_t year = GetYear(days);
   if (year != (ORIGINYEAR + 1))
   {
      cout << "test_firstDayAfterLeapYear failed.  Expected " << 
	     (ORIGINYEAR + 1) << ", actual " << year << endl;
   }
   else
   {
      cout << "test_firstDayAfterLeapYear passed." << endl;
   }
}
