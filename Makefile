.PHONY: all zune example clean

all: zune example

zune:
	@$(MAKE) -C ZuneBug all

example:
	@$(MAKE) -C Utility all

clean:
	@$(MAKE) -C ZuneBug clean
	@$(MAKE) -C Utility clean
	@$(MAKE) -C cpputest -if Makefile_using_MakefileWorker cleanExtensions
